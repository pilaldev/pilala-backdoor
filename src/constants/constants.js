export const AUTH_ERRORS = {
	USER_NOT_FOUND: 'auth/user-not-found',
	WRONG_PASSWORD: 'auth/wrong-password',
	WRONG_EMAIL: 'auth/invalid-email'
};

export const AUTH_MESSAGES = {
	USER_NOT_FOUND: "Sorry, we couldn't find an account with that email",
	WRONG_PASSWORD: 'The password is wrong. Write it again',
	WRONG_EMAIL: 'The email is wrong. Make sure you typed a valid email address',
	NO_DATA_PASSWORD: 'Please enter a valid value for password'
};

export const CONSOLE_SIDEBAR_ITEMS = {
	PENDING_USERS: 'Pending Users',
	INVITED_USERS: 'Invited Users',
	ADD_USERS_MANUALLY: 'Add users manually',
	UPLOAD_USERS: 'Upload users',
	UPLOAD_RESOURCES: 'Upload resources',
	READ_RESOURCES: 'Read resources',
	SIGN_OUT: 'Sign out'
};

export const ADD_USER_FORM_ERROR_MESSAGES = {
	FIELD_REQUIRED: 'Este campo es obligatorio',
	USER_EXISTS: 'Ya existe un usuario con este correo',
	REQUEST_FAILED: 'La solicitud falló. Intente de nuevo'
};

export const ID_VALUES = [ 'CC', 'C.E' ];

export const REG_EXPS = {
	name: /^[A-Za-zÁÉÍÓÚñáéíóúÑ ]*$/,
	numbers: /^[0-9]*$/,
	email: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$/
};

