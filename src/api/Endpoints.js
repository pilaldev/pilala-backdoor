const develop = {
	writeUsersData: 'https://us-central1-pilala-develop.cloudfunctions.net/writeUsersData',
	readResources: 'https://us-central1-pilala-develop.cloudfunctions.net/readResources',
	writeResourcesData: 'https://us-central1-pilala-develop.cloudfunctions.net/writeResourcesData',
	signInServerSide: 'https://us-central1-pilala-develop.cloudfunctions.net/signInClient',
	getResources: 'https://us-central1-pilala-develop.cloudfunctions.net/getResources'
};

const qa = {
	writeUsersData: 'https://us-central1-pilala-qa.cloudfunctions.net/writeUsersData',
	readResources: 'https://us-central1-pilala-qa.cloudfunctions.net/readResources',
	writeResourcesData: 'https://us-central1-pilala-qa.cloudfunctions.net/writeResourcesData',
	signInServerSide: 'https://us-central1-pilala-qa.cloudfunctions.net/signInClient',
	getResources: 'https://us-central1-pilala-qa.cloudfunctions.net/getResources',
};

const production = {
	writeUsersData: 'https://us-central1-pilala-b25de.cloudfunctions.net/writeUsersData',
	readResources: 'https://us-central1-pilala-b25de.cloudfunctions.net/readResources',
	writeResourcesData: 'https://us-central1-pilala-b25de.cloudfunctions.net/writeResourcesData',
	signInServerSide: 'https://us-central1-pilala-b25de.cloudfunctions.net/signInClient',
	getResources: 'https://us-central1-pilala-b25de.cloudfunctions.net/getResources',
};

const URLs =
	process.env.REACT_APP_ENV === 'PROD'
		? production
		: process.env.REACT_APP_ENV === 'QA' ? qa : process.env.REACT_APP_ENV === 'DEV' ? develop : develop;

export { URLs as ENDPONINTS };
