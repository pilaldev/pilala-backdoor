import { ENDPONINTS } from '../api/Endpoints';

export const signInServerSide = async (data, token) => {
	console.log('DATA', data, token);
	return new Promise(async (resolve, reject) => {
		await fetch(ENDPONINTS.signInServerSide, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify(data)
		})
			.then((response) => response.json())
			.then((result) => {
				resolve(result);
			})
			.catch((error) => {
				reject({
					status: 'FAILED',
					message: 'Error ejecutanto el servicio de autenticación',
					error
				});
			});
	});
};
