import { ENDPONINTS } from '../api/Endpoints';

export const writeUsersData = (data) => {
	return new Promise((resolve, reject) => {
		fetch(ENDPONINTS.writeUsersData, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(data)
		})
			.then((response) => response.json())
			.then((result) => {
				resolve(result);
			})
			.catch((error) => {
				reject({
					status: 'FAILED',
					message: 'Error enviando la información del usuario',
					error
				});
			});
	});
};
