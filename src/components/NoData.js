import React from 'react';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
// import Icon from '@material-ui/core/Icon';
import './styles/NoDataScreen.css';

const NoDataScreen = () => {
	return (
		<div className="noDataScreen">
			<ErrorOutlineIcon className="material-icons.md-48 NoDataIcon" />
			<span>NO DATA</span>
		</div>
	);
};

export default NoDataScreen;
