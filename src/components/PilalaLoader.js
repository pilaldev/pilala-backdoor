import React from 'react';
import PulseBubbleLoader from 'react-loaders-kit/lib/pulseBubble/PulseBubbleLoader';
import { Typography, makeStyles } from '@material-ui/core';

const loaderProps = {
	loading: true,
	size: 150,
	colors: ['#6A32B5', '#6A32B5', '#6A32B5']
}

const PilalaLoader = (props) => {
	const classes = useStyles();

	return (
		<div className={classes.container}>
			<PulseBubbleLoader {...loaderProps} />
			<div className="status_message">
				<Typography className={classes.message}>
					{props.message}
				</Typography>
			</div>
		</div>
	);
};

const useStyles = makeStyles({
	container: {
		display: 'flex',
		flex: 1,
		flexDirection: 'column',
		height: '100vh',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#FFFFFF'
	},
	message: {
		fontSize: '1.2rem',
		color: '#6A32B5',
		fontWeight: 'bold'
	}
});

export default React.memo(PilalaLoader);
