const formatProvincesArray = (provinces) => {
	return provinces.map((item) => ({
		code: item.code,
		display: item.province,
		value: item.province.toLowerCase()
	}));
};

const formatCitiesArray = (cities) => {
	return cities.map((item) => ({
		code: item.cityCode,
		display: item.city,
		provinceCode: item.provinceCode,
		value: item.city.toLowerCase()
	}));
};

export default {
	formatProvincesArray,
	formatCitiesArray
};
