import { createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import rootReducer from './reducers';

const isDevelop = process.env.REACT_APP_ENV === 'DEV';
const persistConfig = {
	key: 'root',
	storage,
	// whitelist: ['loginForm'],
	blacklist: [ 'loginForm', 'addUserForm', 'usersList' ],
	debug: isDevelop
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(
	persistedReducer,
	isDevelop
		? window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__({ trace: true, traceLimit: 25 })
		: undefined
);

export const persistor = persistStore(store);
