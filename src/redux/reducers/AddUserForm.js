import { ADD_USER_DATA_FORM } from '../actions/types';

const initialState = {
	fullName: '',
	email: '',
	documentType: 'CC',
	documentNumber: '',
	phoneNumber: '',
	companyNit: '',
	province: '',
	city: '',
	genre: {
		display: 'Masculino',
		value: 1
	},
	featureFlags: {
		ccCompany: true,
		ccSucursal: false,
		ccCotizante: true,
		ccPayroll: true,
		ccPayrollCorrection: false,
		ccMultiCompany: true,
		ccLoadPayrollFile: false
	},
	errorMessages: {
		fullName: '',
		lastName: '',
		email: '',
		documentType: '',
		documentNumber: '',
		phoneNumber: '',
		companyNit: ''
	}
};

const saveAddUserData = (state = initialState, action) => {
	switch (action.type) {
		case ADD_USER_DATA_FORM.RESET_DATA:
			return {
				...state,
				fullName: '',
				email: '',
				documentType: 'CC',
				documentNumber: '',
				phoneNumber: '',
				companyNit: '',
				errorMessages: {
					name: '',
					lastName: '',
					email: '',
					documentType: '',
					documentNumber: '',
					phoneNumber: '',
					companyNit: ''
				}
			};

		case ADD_USER_DATA_FORM.SET_USER_DATA:
			return {
				...state,
				[action.payload.field]: action.payload.value,
				errorMessages: {
					...state.errorMessages,
					[action.payload.field]: ''
				}
			};

		case ADD_USER_DATA_FORM.SET_FEATURE_FLAG_VALUE:
			return {
				...state,
				featureFlags: {
					...state.featureFlags,
					[action.payload.value.field]: !state.featureFlags[action.payload.value.field]
				}
			};

		case ADD_USER_DATA_FORM.SET_ERROR_MESSAGE:
			return {
				...state,
				errorMessages: {
					...state.errorMessages,
					[action.payload.field]: action.payload.value
				}
			};

		default:
			return state;
	}
};

export default saveAddUserData;
