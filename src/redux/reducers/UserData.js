import { USER_DATA } from '../actions/types';

const initialState = {
	loginStatus: false,
	uid: '',
	name: '',
	email: ''
};

const saveUserData = (state = initialState, action) => {
	switch (action.type) {
		case USER_DATA.SET_USER_DATA:
			return {
				...state,
				uid: action.payload.value.uid ? action.payload.value.uid : '',
				name: action.payload.value.name ? action.payload.value.name : '',
				email: action.payload.value.email ? action.payload.value.email : ''
			};

		case USER_DATA.SET_LOGIN_STATUS:
			return {
				...state,
				loginStatus: action.payload.value
			};

		default:
			return state;
	}
};

export default saveUserData;
