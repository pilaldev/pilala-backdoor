import { UPLOAD_RESOURCES } from '../actions/types';

const initialState = {
	selectedOption: null,
	requestResult: ''
};

const uploadResources = (state = initialState, action) => {
	switch (action.type) {
		case UPLOAD_RESOURCES.UPLOAD_RESOURCES_SET_SELECTED_OPTION:
			return {
				...state,
				selectedOption: action.payload.value
			};

		case UPLOAD_RESOURCES.UPLOAD_RESOURCES_SET_REQUEST_RESULT:
			return {
				...state,
				requestResult: action.payload.value
			};

		default:
			return state;
	}
};

export default uploadResources;
