import { FETCH_DATA } from './../actions/types';

const initialState = {
	requestLists: 'PENDING'
};

const FetchData = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_DATA.SET_REQUEST_STATUS:
			return {
				...state,
				[action.payload.field]: action.payload.value
			};

		case FETCH_DATA.RESET_REQUEST_STATUS:
			return {
				...state,
				...initialState
			};

		default:
			return state;
	}
};

export default FetchData;
