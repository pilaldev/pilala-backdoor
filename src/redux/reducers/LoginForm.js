import { LOGIN_FORM } from '../actions/types';

const initialState = {
	email: '',
	password: '',
	emailError: false,
	passwordError: false
};

const saveLoginData = (state = initialState, action) => {
	switch (action.type) {
		case LOGIN_FORM.RESET_DATA:
			return {
				...state,
				email: '',
				password: '',
				errorEmail: false,
				errorPassword: false
			};

		case LOGIN_FORM.SET_EMAIL:
			return {
				...state,
                email: action.payload.value,
                emailError: false,
			};

		case LOGIN_FORM.SET_PASSWORD:
			return {
				...state,
                password: action.payload.value,
                passwordError: false
			};

		case LOGIN_FORM.SET_PASSWORD_ERROR:
			return {
				...state,
				passwordError: action.payload.value
			};

		case LOGIN_FORM.SET_EMAIL_ERROR:
			return {
				...state,
				emailError: action.payload.value
			};

		default:
			return state;
	}
};

export default saveLoginData;
