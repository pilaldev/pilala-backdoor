import { USERS_LIST } from '../actions/types';

const initialState = {
	pendingUsersSnapshot: false,
	invitedUsersSnapshot: false,
	pendingUsers: {},
	invitedUsers: {}
};

const saveUserData = (state = initialState, action) => {
	switch (action.type) {
		case USERS_LIST.SET_SNAPSHOT_STATUS:
			return {
				...state,
				[action.payload.field]: action.payload.value
			};

		case USERS_LIST.SET_LIST_DATA:
			return {
				...state,
				[action.payload.field]: {
					...state[action.payload.field],
					...action.payload.value
				}
			};

		case USERS_LIST.DELETE_USER:
			const { [action.payload.email]: value, ...deletedUser } = state[action.payload.field];

			return {
				...state,
				[action.payload.field]: deletedUser
			};

		default:
			return state;
	}
};

export default saveUserData;
