import { RESOURCES } from './../actions/types';

const initialState = {
	cities: [],
	provinces: []
};

const saveResources = (state = initialState, action) => {
	switch (action.type) {
		case RESOURCES.RESOURCES_SAVE_LOCATION_DATA:
			return {
				...state,
				cities: action.payload.value.cities,
				provinces: action.payload.value.provinces
			};

		case RESOURCES.RESOURCES_RESET_DATA:
			return {
				...state,
				...initialState
			};

		default:
			return state;
	}
};

export default saveResources;
