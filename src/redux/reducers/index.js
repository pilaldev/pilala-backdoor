import { combineReducers } from 'redux';
// import { persistReducer } from 'redux-persist';
// import storage from 'redux-persist/lib/storage';

import LoginForm from '../reducers/LoginForm';
import UserData from '../reducers/UserData';
import AddUserForm from '../reducers/AddUserForm';
import UsersList from '../reducers/UsersList';
import UploadResources from '../reducers/UploadResources';
import FetchData from '../reducers/FetchData';
import Resources from '../reducers/Resources';

// const loginConfig = {
// 	key: LoginForm,
// 	storage: storage,
//     timeout: null,
//     whitelist:[
//         'password'
//     ]
// };

export default combineReducers({
	// loginForm: persistReducer(loginConfig, LoginForm),
	loginForm: LoginForm,
	userData: UserData,
	addUserForm: AddUserForm,
	usersList: UsersList,
	uploadResources: UploadResources,
	fetchData: FetchData,
	resources: Resources
});
