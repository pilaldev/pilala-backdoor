import { ADD_USER_DATA_FORM } from '../actions/types';

export const setAddUserData = (field, value) => {
	return {
		type: ADD_USER_DATA_FORM.SET_USER_DATA,
		payload: {
			field,
			value
		}
	};
};

export const setErrorMessage = (field, value) => {
	return {
		type: ADD_USER_DATA_FORM.SET_ERROR_MESSAGE,
		payload: {
			field,
			value
		}
	};
};

export const resetAddUserData = () => {
	return {
		type: ADD_USER_DATA_FORM.RESET_DATA
	};
};
