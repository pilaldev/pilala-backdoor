import {LOGIN_FORM} from "../actions/types"

export const setLoginData = (actionType,value) => {
    return {
        type: actionType,
        payload: {
            value
        }
    }
}

export const resetLoginData = () => {
    return {
        type: LOGIN_FORM.RESET_DATA,
    }
}



