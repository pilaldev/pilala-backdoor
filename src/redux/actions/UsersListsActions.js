import { USERS_LIST } from '../actions/types';

export const setSnapshotStatus = (field, value) => {
	return {
		type: USERS_LIST.SET_SNAPSHOT_STATUS,
		payload: {
			field,
			value
		}
	};
};

export const setListData = (field, value) => {
	return {
		type: USERS_LIST.SET_LIST_DATA,
		payload: {
			field,
			value
		}
	};
};

export const deleteUser = (field, email) => {
	return {
		type: USERS_LIST.DELETE_USER,
		payload: {
			field,
			email
		}
	};
};