export const uploadResourcesAction = (actionType, value) => {
	return {
		type: actionType,
		payload: {
			value
		}
	};
};

/* Fetch Data*/

export const fetchDataAction = (actionType, field, value) => {
	return {
		type: actionType,
		payload: { field, value }
	};
};

/* Resources */
export const resourcesAction = (actionType, value) => {
	return {
		type: actionType,
		payload: {
			value
		}
	};
};


/* Add user form actions */

export const addUserFormAction = (actionType, value) => {
	return {
		type: actionType,
		payload: {
			value
		}
	};
};
