import { USER_DATA } from '../actions/types';

export const setUserData = (value) => {
	return {
		type: USER_DATA.SET_USER_DATA,
		payload: {
			value
		}
	};
};

export const setLoginStatus = (value) => {
	return {
		type: USER_DATA.SET_LOGIN_STATUS,
		payload: {
			value
		}
	};
};
