import React, { Component } from 'react';

import { Provider } from 'react-redux';
import { store, persistor } from './redux/store';
import { PersistGate } from 'redux-persist/integration/react';
import MainContainer from './containers/MainContainer';
// import { BrowserRouter } from 'react-router-dom';

import './App.css';

class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<PersistGate loading={null} persistor={persistor}>
					<MainContainer />
				</PersistGate>
			</Provider>
		);
	}
}

export default App;
