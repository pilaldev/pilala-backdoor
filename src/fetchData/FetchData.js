import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchDataAction, resourcesAction } from '../redux/actions/Actions';
import { FETCH_DATA, RESOURCES } from '../redux/actions/types';
import { getResources } from './FetchDataApi';
import PilalaLoader from '../components/PilalaLoader';
import HelperFunctions from './../utils/HelperFunctions';
import { LOADER_MESSAGE } from './Constants';

class FetchData extends Component {
	componentDidMount() {
		if (this.props.requestLists === 'SUCESSS') {
			this.props.history.replace(`/${this.props.userUid}/Console`);
		} else {
			this.getRemoteData();
		}
	}

	componentDidUpdate() {
		if (this.props.requestLists !== 'PENDING') {
			if (this.props.requestLists === 'SUCCESS') {
				this.props.history.replace(`/${this.props.userUid}/Console`);
			} else {
				// TODO: Plantear una redirección o re-ejecución de las solicitudes en caso de que alguna falle cuando ya se tienen compañias creadas
				alert('Ocurrio un error en las solicitudes iniciales');
			}
		}
	}

	getRemoteData = async () => {
		if (this.props.userUid) {
			await this.requestLists();
		}
	};

	requestLists = async () => {
		await getResources()
			.then((result) => {
				if (result.status === 'SUCCESS') {
					this.props.resourcesAction(RESOURCES.RESOURCES_SAVE_LOCATION_DATA, {
						cities: HelperFunctions.formatCitiesArray(result.cities),
						provinces: HelperFunctions.formatProvincesArray(result.provinces)
					});
					this.props.fetchDataAction(FETCH_DATA.SET_REQUEST_STATUS, 'requestLists', 'SUCCESS');
				} else {
					this.props.fetchDataAction(FETCH_DATA.SET_REQUEST_STATUS, 'requestLists', 'FAILED');
				}
			})
			.catch(() => {
				this.props.fetchDataAction(FETCH_DATA.SET_REQUEST_STATUS, 'requestLists', 'FAILED');
			});
	};

	render() {
		return <PilalaLoader message={LOADER_MESSAGE} />;
	}
}

const mapStateToProps = (state) => {
	return {
		userUid: state.userData.uid,
		requestLists: state.fetchData.requestLists
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		fetchDataAction: (actionType, field, value) => dispatch(fetchDataAction(actionType, field, value)),
		resourcesAction: (actionType, value) => dispatch(resourcesAction(actionType, value))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(FetchData);
