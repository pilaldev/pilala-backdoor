import { ENDPONINTS } from '../api/Endpoints';
import { FAILED } from './Constants';

export const getResources = async () => {
	return new Promise(async (resolve, reject) => {
		await fetch(ENDPONINTS.getResources, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		})
			.then((response) => response.json())
			.then((result) => {
				resolve(result);
			})
			.catch((error) => {
				reject({
					status: FAILED,
					message: 'Error ejecutanto la CF de departamentos y ciudades desde el front',
					error
				});
			});
	});
};
