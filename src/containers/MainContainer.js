import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { setUserData, setLoginStatus } from '../redux/actions/UserDataActions';
import LoginContainer from '../invitationPanel/containers/LoginContainer';
import Console from '../invitationPanel/containers/ConsoleContainer';
import NotFound from '../components/NotFound';
import FetchData from '../fetchData/FetchData'

class MainContainer extends Component {
	render() {
		return (
			<Router>
				<Switch>
					<Route exact path="/" component={LoginContainer} />
					<Route path="/:userId/Console/" component={Console} />
					<Route path="/:userId/fetchData/" component={FetchData} />
					<Route component={NotFound} />
				</Switch>
			</Router>
		);
	}
}

const mapStateToProps = (state) => {
	return {};
};

const mapDispatchToProps = (dispatch) => {
	return {
		setUserData: (value) => dispatch(setUserData(value)),
		setLoginStatus: (value) => dispatch(setLoginStatus(value))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer);
