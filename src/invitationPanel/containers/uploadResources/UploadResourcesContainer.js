import React, { Component } from 'react';
import { connect } from 'react-redux';
// import StatusUsersLayout from './StatusUsersLayout';
// import { FIRESTORE } from '../../../api/Firebase';
// import { setSnapshotStatus, setListData, deleteUser } from '../../../redux/actions/UsersListsActions';
import FlatfileImporter from 'flatfile-csv-importer';
import UploadResourcesLayout from './UploadResourcesLayout';
import { uploadResourcesAction } from '../../../redux/actions/Actions';
import { UPLOAD_RESOURCES } from '../../../redux/actions/types';
import { FIELD_CONFIGURATION } from './constants';
import { writeResourcesData } from './API/WriteResources';
const LICENSE_KEY = 'd55b2a8a-8e65-458d-b6f7-2f9e44be1397';

FlatfileImporter.setVersion(2);

const DEFAULTS = {
	allowInvalidSubmit: true,
	managed: true
};

export class UploadResourcesContainer extends Component {
	handleSelectedOption = (selectedOption) => {
		this.props.uploadResourcesAction(UPLOAD_RESOURCES.UPLOAD_RESOURCES_SET_SELECTED_OPTION, selectedOption);
	};

	uploadData = async () => {
		const importer = new FlatfileImporter(LICENSE_KEY, {
			...DEFAULTS,
			...FIELD_CONFIGURATION[this.props.selectedOption]
		});

		importer.setCustomer({
			userId: this.props.userUid,
			name: this.props.userName + ' ' + this.props.lastName
		});

		importer
			.requestDataFromUser()
			.then((results) => {
				importer.displayLoader();

				let requestData = {
					resourceType: this.props.selectedOption,
					data: results.validData,
					options: {}
				};

				writeResourcesData(requestData)
					.then((result) => {
						this.props.uploadResourcesAction(
							UPLOAD_RESOURCES.UPLOAD_RESOURCES_SET_REQUEST_RESULT,
							JSON.stringify(result)
						);
						importer.displaySuccess(result.message);
					})
					.catch((error) => {
						this.props.uploadResourcesAction(
							UPLOAD_RESOURCES.UPLOAD_RESOURCES_SET_REQUEST_RESULT,
							JSON.stringify(error)
						);
						importer.displaySuccess(error.message);
					});
			})
			.catch((error) => {
				this.props.uploadResourcesAction(
					UPLOAD_RESOURCES.UPLOAD_RESOURCES_SET_REQUEST_RESULT,
					JSON.stringify(error)
				);
				importer.displaySuccess(error);
			});
	};

	render() {
		const layoutProps = {
			handleSelectedOption: this.handleSelectedOption,
			selectedOption: this.props.selectedOption,
			uploadData: this.uploadData,
			requestResult: this.props.requestResult
		};

		return <UploadResourcesLayout {...layoutProps} />;
	}
}

const mapStateToProps = (state) => {
	return {
		selectedOption: state.uploadResources.selectedOption,
		userUid: state.userData.uid,
		userName: state.userData.name,
		lastName: state.userData.lastName,
		requestResult: state.uploadResources.requestResult
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		uploadResourcesAction: (actionType, value) => dispatch(uploadResourcesAction(actionType, value))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(UploadResourcesContainer);
