import { ENDPONINTS } from '../../../../api/Endpoints';

export const writeResourcesData = async (data) => {
	return new Promise((resolve, reject) => {
		fetch(ENDPONINTS.writeResourcesData, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(data)
		})
			.then((response) => response.json())
			.then((result) => {
				resolve(result);
			})
			.catch((error) => {
				reject({
					status: 'FAILED',
					message: 'Error ejecutanto la CF de escritura de recursos',
					error
				});
			});
	});
};
