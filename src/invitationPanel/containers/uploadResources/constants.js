export const CATEGORIES = {
	SALARIOS: [ 'SMLMV', 'Salario Integral' ],
	ADMINISTRADORAS: [ 'AFP', 'ARL', 'CCF', 'EPS' ],
	LOCATION: [ 'Provinces', 'Cities' ],
	CIUU: [ 'CIUU' ],
	ADDRESS: [ 'tipo Vial', 'tipos Cuadrante' ],
	ARL: [ 'arl Clases' ],
	CAMARAS: [ 'camaras' ],
	APORTANTES: [ 'identificacion Aportante', 'clases Aportante' ],
	COTIZANTES: [ 'subtipos Cotizantes', 'tipos Cotizantes' ]
};

// const FIELD_TEST = {
// 	title: 'Add Users',
// 	fields: [
// 		{
// 			label: 'codigo',
// 			key: 'codigo',
// 			description: 'The full name of the user'
// 		},
// 		{
// 			label: 'logo',
// 			key: 'logo',
// 			description: 'The full name of the user'
// 		},
// 		{
// 			label: 'nit',
// 			key: 'nit',
// 			description: 'The full name of the user'
// 		},
// 		{
// 			label: 'razonSocial',
// 			key: 'razonSocial',
// 			description: 'The full name of the user'
// 		},
// 		{
// 			label: 'subsistema',
// 			key: 'subsistema',
// 			description: 'The full name of the user'
// 		}
// 	],
// 	type: 'User'
// };

const TIPOS_COTIZANTES_FIELDS = {
	title: 'Datos de los tipos de cotizantes',
	fields: [
		{
			label: 'code',
			key: 'code',
			description: 'codigo del tipo de cotizante'
		},
		{
			label: 'display',
			key: 'display',
			description: 'display value del tipo de cotizante'
		},
		{
			label: 'value',
			key: 'value',
			description: 'value del tipo de cotizante'
		}
	],
	type: 'TIPOS_COTIZANTES_DATA'
};

const SUBTIPOS_COTIZANTES_FIELDS = {
	title: 'Datos de los subtipos de cotizantes',
	fields: [
		{
			label: 'code',
			key: 'code',
			description: 'codigo del subtipo de cotizante'
		},
		{
			label: 'display',
			key: 'display',
			description: 'display value del subtipo de cotizante'
		}
	],
	type: 'SUBTIPOS_COTIZANTES_DATA'
};

const CLASES_APORTANTE_FIELDS = {
	title: 'Datos de las clases de aportante',
	fields: [
		{
			label: 'code',
			key: 'code',
			description: 'codigo de la clase de aportante'
		},
		{
			label: 'display',
			key: 'display',
			description: 'display value de la clase de aportante'
		}
	],
	type: 'CLASES_APORTANTE_DATA'
};

const IDENTICACION_APORTANTE_FIELDS = {
	title: 'Datos de los tipos de identificacion de aportante',
	fields: [
		{
			label: 'code',
			key: 'code',
			description: 'codigo del tipo de identificacion de aportante'
		},
		{
			label: 'display',
			key: 'display',
			description: 'display value tipo de identificacion de aportante'
		}
	],
	type: 'IDENTIFICACION_APORTANTE_FIELDS'
};

const CAMARAS_FIELDS = {
	title: 'Datos de las camaras de comercio',
	fields: [
		{
			label: 'province',
			key: 'province',
			description: 'departamente de la camara de comercio'
		},
		{
			label: 'camara',
			key: 'camara',
			description: 'nombre de la camara de comercio'
		},
		{
			label: 'city',
			key: 'city',
			description: 'ciudad de la camara de comercio'
		}
	],
	type: 'CAMARAS_DATA'
};

const ARL_CLASES_FIELDS = {
	title: 'Datos de clases de ARL',
	fields: [
		{
			label: 'label',
			key: 'label',
			description: 'label de la clase de ARL'
		},
		{
			label: 'value',
			key: 'value',
			description: 'valor de la clase de ARL'
		}
	],
	type: 'ARL_CLASES_DATA'
};

const TIPO_VIAL_FIELDS = {
	title: 'Datos tipo vial',
	fields: [
		{
			label: 'codigo',
			key: 'codigo',
			description: 'Codigo del tipo vial'
		},
		{
			label: 'display',
			key: 'display',
			description: 'Display value del tipo vial'
		}
	],
	type: 'TIPO_VIAL_DATA'
};

const TIPO_CUADRANTES_FIELDS = {
	title: 'Datos tipo cuadrante',
	fields: [
		{
			label: 'codigo',
			key: 'codigo',
			description: 'Codigo del cuadrante'
		},
		{
			label: 'display',
			key: 'display',
			description: 'Display value del cuadrante'
		}
	],
	type: 'TIPO_CUADRANTE_DATA'
};

const AFP_FIELDS = {
	title: 'DATOS DE AFP',
	fields: [
		{
			label: 'codigo',
			key: 'codigo',
			description: 'Codigo de la AFP'
		},
		{
			label: 'logo',
			key: 'logo',
			description: 'URL del logo de la AFP'
		},
		{
			label: 'nit',
			key: 'nit',
			description: 'Nit de la AFP'
		},
		{
			label: 'razonSocial',
			key: 'razonSocial',
			description: 'Razon social de la AFP'
		},
		{
			label: 'subsistema',
			key: 'subsistema',
			description: 'Subsistema'
		}
	],
	type: 'AFP_DATA'
};

const ARL_FIELDS = {
	title: 'DATOS DE ARL',
	fields: [
		{
			label: 'codigo',
			key: 'codigo',
			description: 'Codigo de la ARL'
		},
		{
			label: 'logo',
			key: 'logo',
			description: 'URL del logo de la ARL'
		},
		{
			label: 'nit',
			key: 'nit',
			description: 'Nit de la ARL'
		},
		{
			label: 'razonSocial',
			key: 'razonSocial',
			description: 'Razon social de la ARL'
		},
		{
			label: 'subsistema',
			key: 'subsistema',
			description: 'Subsistema'
		}
	],
	type: 'ARL_DATA'
};

const CCF_FIELDS = {
	title: 'DATOS DE CCF',
	fields: [
		{
			label: 'codigo',
			key: 'codigo',
			description: 'Codigo de la CCF'
		},
		{
			label: 'logo',
			key: 'logo',
			description: 'URL del logo de la CCF'
		},
		{
			label: 'nit',
			key: 'nit',
			description: 'Nit de la CCF'
		},
		{
			label: 'razonSocial',
			key: 'razonSocial',
			description: 'Razon social de la CCF'
		},
		{
			label: 'subsistema',
			key: 'subsistema',
			description: 'Subsistema'
		},
		{
			label: 'departamento',
			key: 'departamento',
			description: 'Departamento'
		}
	],
	type: 'CCF_DATA'
};

const EPS_FIELDS = {
	title: 'DATOS DE EPS',
	fields: [
		{
			label: 'codigo',
			key: 'codigo',
			description: 'Codigo de la EPS'
		},
		{
			label: 'logo',
			key: 'logo',
			description: 'URL del logo de la EPS'
		},
		{
			label: 'nit',
			key: 'nit',
			description: 'Nit de la EPS'
		},
		{
			label: 'razonSocial',
			key: 'razonSocial',
			description: 'Razon social de la EPS'
		},
		{
			label: 'subsistema',
			key: 'subsistema',
			description: 'Subsistema'
		}
	],
	type: 'EPS_DATA'
};

const SMLMV_FIELDS = {
	title: 'Valor de SMLMV',
	fields: [
		{
			label: 'SMLMV',
			key: 'SMLMV',
			description: 'Valor del SMLMV'
		}
	],
	type: 'SMLMV_DATA',
	maxRecords: 1
};

const SI_FIELDS = {
	title: 'Valor de salario integral',
	fields: [
		{
			label: 'Salario Integral',
			key: 'Salario Integral',
			description: 'Valor del salario integral'
		}
	],
	type: 'SI_DATA',
	maxRecords: 1
};

const CITIES_FIELDS = {
	title: 'CITIES DATA',
	fields: [
		{
			label: 'city',
			key: 'city',
			description: 'City Name'
		},
		{
			label: 'cityCode',
			key: 'cityCode',
			description: 'The code of the City'
		},
		{
			label: 'province',
			key: 'province',
			description: 'Province'
		},
		{
			label: 'provinceCode',
			key: 'provinceCode',
			description: 'The code of the province'
		}
	],
	type: 'City'
};

const PROVINCES_FIELDS = {
	title: 'PROVINCES DATA',
	fields: [
		{
			label: 'code',
			key: 'code',
			description: 'Province Code'
		},
		{
			label: 'province',
			key: 'province',
			description: 'Province'
		}
	],
	type: 'Province'
};

const CIIU_FIELDS = {
	title: 'CIIU DATA',
	fields: [
		{
			label: 'code',
			key: 'code',
			description: 'CIIU Code'
		},
		{
			label: 'description',
			key: 'description',
			description: 'Activity description'
		}
	],
	type: 'CIIU'
};

export const FIELD_CONFIGURATION = {
	SMLMV: SMLMV_FIELDS,
	'Salario Integral': SI_FIELDS,
	AFP: AFP_FIELDS,
	ARL: ARL_FIELDS,
	CCF: CCF_FIELDS,
	EPS: EPS_FIELDS,
	Provinces: PROVINCES_FIELDS,
	Cities: CITIES_FIELDS,
	CIUU: CIIU_FIELDS,
	'tipo Vial': TIPO_VIAL_FIELDS,
	'tipos Cuadrante': TIPO_CUADRANTES_FIELDS,
	'arl Clases': ARL_CLASES_FIELDS,
	camaras: CAMARAS_FIELDS,
	'clases Aportante': CLASES_APORTANTE_FIELDS,
	'identificacion Aportante': IDENTICACION_APORTANTE_FIELDS,
	'subtipos Cotizantes': SUBTIPOS_COTIZANTES_FIELDS,
	'tipos Cotizantes': TIPOS_COTIZANTES_FIELDS
};
