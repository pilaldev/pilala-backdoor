import React from 'react';
import { makeStyles, createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import ListSubheader from '@material-ui/core/ListSubheader';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

import Button from '@material-ui/core/Button';
import { CATEGORIES } from './constants';

const theme = createMuiTheme({
	palette: {
		primary: {
			main: '#3f51b5'
		},
		secondary: {
			main: '#d81b60'
		}
	}
});

const useStyles = makeStyles({
	container: {
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center'
	},
	textArea: {
		width: '80%',
		padding: 64,
		margin: '32px 0',
		border: '1px solid #c1c6d1',
		borderRadius: 4,
		backgroundColor: '#3c4151',
		color: '#fff',
		height: 200,
		overflow: 'hidden',
		fontSize: 18
	},
	formControl: {
		margin: theme.spacing(1),
		minWidth: 200
	}
});

const UploadResourcesLayout = (props) => {
	const classes = useStyles();

	return (
		<ThemeProvider theme={theme}>
			<div className={classes.container}>
				<FormControl className={classes.formControl} variant="filled">
					<InputLabel>Recursos</InputLabel>
					<Select
						value={props.selectedOption}
						onChange={(e) => {
							props.handleSelectedOption(e.target.value);
						}}
					>
						<MenuItem value={null}>
							<em>None</em>
						</MenuItem>
						{Object.keys(CATEGORIES).map((category) => {
							return [
								<ListSubheader>{category}</ListSubheader>,
								CATEGORIES[category].map((item) => {
									return <MenuItem value={item}>{item}</MenuItem>;
								})
							];
						})}
					</Select>
				</FormControl>

				<Button variant="contained" color="primary" disabled={!props.selectedOption} onClick={props.uploadData}>
					SUBIR
				</Button>

				<textarea className={classes.textArea} readOnly value={props.requestResult} />
			</div>;
		</ThemeProvider>
	);
};

export default UploadResourcesLayout;
