import React from 'react';
import { makeStyles, createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import './cardsLayout.css';

import NoDataScreen from '../../../components/NoData';

const theme = createMuiTheme({
	palette: {
		primary: {
			main: '#34eb3d'
		},
		secondary: {
			main: '#eb4634'
		}
	}
});

const useStyles = makeStyles({
	root: {
		width: 230,
		height: 330,
		marginLeft: 20,
		marginTop: 5
	},
	cardActionArea: {
		height: 280
	},
	media: {
		height: 110,
		width: 'auto',
		backgroundSize: 'contain'
	},
	infoContent: {
		height: 160
	},
	actionsContainer: {
		height: 50
	}
});

const StatusUsersLayout = (props) => {
	const classes = useStyles();

	return (
		<div className="cardsContainer">
			{Object.keys(props.users).length > 0 ? (
				Object.keys(props.users).map((key, index) => {
					let randonNumber = Math.ceil(Math.random() * 16);
					return (
						<ThemeProvider theme={theme} key={index}>
							<Card className={classes.root}>
								<CardActionArea className={classes.cardActionArea}>
									<CardMedia
										className={classes.media}
										image={require(`./../../../assets/images/avatars/avatar_${randonNumber.toString()}.svg`)}
										title="User"
									/>

									<CardContent className={classes.infoContent}>
										<Typography gutterBottom variant="h6" component="h2">
											{props.users[key].name}
										</Typography>
										<Typography variant="body2" color="textSecondary" component="p">
											Email: {key}
										</Typography>
										<Typography variant="body2" color="textSecondary" component="p">
											Invited: {props.users[key].invited.toString()}
										</Typography>
									</CardContent>
								</CardActionArea>
								<CardActions className={classes.actionsContainer}>
									<Button size="small" color="secondary" onClick={() => props.deleteUser(key)}>
										DELETE
									</Button>

									<Button size="small" color="primary" onClick={() => props.inviteUser(key)}>
										INVITE
									</Button>
								</CardActions>
							</Card>
						</ThemeProvider>
					);
				})
			) : (
				<NoDataScreen />
			)}
		</div>
	);
};

export default StatusUsersLayout;
