import React, { Component } from 'react';
import { connect } from 'react-redux';
import StatusUsersLayout from './StatusUsersLayout';
import { FIRESTORE } from '../../../api/Firebase';
import { setSnapshotStatus, setListData, deleteUser } from '../../../redux/actions/UsersListsActions';
// import PilalaLoader from '../../../components/PilalaLoader';

export class StatusUsers extends Component {
	componentDidMount() {
		if (!this.props.pendingUsersSnapshot) {
			this.props.setSnapshotStatus('pendingUsersSnapshot', true);
			FIRESTORE.collection('users').where('invited', '==', false).onSnapshot((docs) => {
				let users = {};
				
				docs.docChanges().forEach((change) => {

				
					if (change.type === 'added') {
					
						users[change.doc.data().email] = { ...change.doc.data() };
					}
					// if (change.type === 'modified') {
					// 	console.log("modified")
					// }
					// if (change.type === "removed") {
					// 	console.log("Removed city: ", change.doc.data());
					// }
				});

				
				if (Object.keys(users).length) {
					this.props.setListData('pendingUsers', users);
				}
			});
		}
	}

	deleteUser = (email) => {
	
		FIRESTORE.collection('users').doc(this.props.pendingUsers[email].email).delete().then((ref) => {
			this.props.deleteUser('pendingUsers', email);
		});
	};

	inviteUser = (email) => {
		let userEmail = email;
		FIRESTORE.collection('users')
			.doc(this.props.pendingUsers[userEmail].email)
			.update({
				invited: true
			})
			.then((ref) => {
			
				this.props.deleteUser('pendingUsers', userEmail);
			});
	};

	render() {
		const layoutProps = {
			users: this.props.pendingUsers,
			inviteUser: this.inviteUser,
			deleteUser: this.deleteUser
		};

		return <StatusUsersLayout {...layoutProps} />;
	}
}

const mapStateToProps = (state) => {
	return {
		pendingUsers: state.usersList.pendingUsers,
		pendingUsersSnapshot: state.usersList.pendingUsersSnapshot
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		setSnapshotStatus: (field, value) => dispatch(setSnapshotStatus(field, value)),
		setListData: (field, value) => dispatch(setListData(field, value)),
		deleteUser: (field, email) => dispatch(deleteUser(field, email))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(StatusUsers);
