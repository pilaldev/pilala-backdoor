import React, { Component } from 'react';
import { connect } from 'react-redux';
import InvitedUsersLayout from './InvitedUsersLayout';
import { FIRESTORE } from '../../../api/Firebase';
import { setSnapshotStatus, setListData, deleteUser } from '../../../redux/actions/UsersListsActions';
// import PilalaLoader from '../../../components/PilalaLoader';

export class InvitedUsers extends Component {
	state = {
		users: []
	};

	componentDidMount() {
		if (!this.props.invitedUsersSnapshot) {
			this.props.setSnapshotStatus('invitedUsersSnapshot', true);
			FIRESTORE.collection('users').where('invited', '==', true).onSnapshot((docs) => {
				let users = {};
			
				docs.docChanges().forEach((change) => {
					if (change.type === 'added') {
			
						users[change.doc.data().email] = { ...change.doc.data() };
					}
					if (change.type === 'modified') {
						users[change.doc.data().email] = { ...change.doc.data() };
					}
					// if (change.type === "removed") {
					// 	console.log("Removed city: ", change.doc.data());
					// }
				});

			
				if (Object.keys(users).length) {
					this.props.setListData('invitedUsers', users);
				}
			});
		}
	}

	deleteUser = (email) => {
		FIRESTORE.collection('users').doc(this.props.invitedUsers[email].email).delete().then((ref) => {
			this.props.deleteUser('invitedUsers', email);
		});
	};

	render() {
		const layoutProps = {
			users: this.props.invitedUsers,
			deleteUser: this.deleteUser
		};

		return <InvitedUsersLayout {...layoutProps} />;
	}
}

const mapStateToProps = (state) => {
	return {
		invitedUsers: state.usersList.invitedUsers,
		invitedUsersSnapshot: state.usersList.invitedUsersSnapshot
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		setSnapshotStatus: (field, value) => dispatch(setSnapshotStatus(field, value)),
		setListData: (field, value) => dispatch(setListData(field, value)),
		deleteUser: (field, email) => dispatch(deleteUser(field, email))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(InvitedUsers);
