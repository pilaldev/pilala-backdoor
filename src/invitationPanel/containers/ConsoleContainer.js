import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FIREBASE_AUTH } from '../../api/Firebase';
import { CONSOLE_SIDEBAR_ITEMS } from '../../constants/constants';
import ConsoleLayout from '../containers/ConsoleLayout';

import AddUsers from '../containers/addUsers/AddUsersContainer';
import StatusUsers from '../containers/statusUsers/StatusUsersContainer';
import InvitedUsers from '../containers/invitedUsers/InvitedUsersContainer';
import UploadUsers from '../containers/uploadUsers/UploadUsersContainer';

import PilalaLoader from '../../components/PilalaLoader';
import UploadResourcesContainer from '../containers/uploadResources/UploadResourcesContainer';
import ReadResources from './readResources/ReadResources';

import { setUserData, setLoginStatus } from '../../redux/actions/UserDataActions';
import { fetchDataAction, resourcesAction} from '../../redux/actions/Actions';
import { FETCH_DATA,RESOURCES} from '../../redux/actions/types';

export class ConsoleContainer extends Component {
	navigateToSections = (text) => {
		let array = Object.keys(CONSOLE_SIDEBAR_ITEMS);

		switch (text) {
			case array[0]:
				this.props.history.replace(`/${this.props.userUid}/Console`);

				break;

			case array[1]:
				this.props.history.replace(`/${this.props.userUid}/Console/InvitedUsers`);

				break;

			case array[2]:
				this.props.history.replace(`/${this.props.userUid}/Console/AddUsers`);

				break;

			case array[3]:
				this.props.history.replace(`/${this.props.userUid}/Console/UploadUsers`);

				break;

			case array[4]:
				this.props.history.replace(`/${this.props.userUid}/Console/UploadResources`);

				break;

			case array[5]:
				this.props.history.replace(`/${this.props.userUid}/Console/ReadResources`);

				break;

			case array[6]:
				FIREBASE_AUTH.signOut().then((ref) => {
					this.clearReduxOnSignOut();
					this.props.setLoginStatus(false);
					this.props.setUserData({});
					this.props.history.replace(`/`);
				});

				break;
			default:
				break;
		}
	};

	clearReduxOnSignOut = () => {
		this.props.fetchDataAction(FETCH_DATA.RESET_REQUEST_STATUS, '', {});
		this.props.resourcesAction(RESOURCES.RESOURCES_RESET_DATA,{})
		this.props.setUserData({});
	};

	render() {
		const layoutProps = {
			navigateToSections: this.navigateToSections
		};

		return this.props.loginStatus ? this.props.match.params.userId === this.props.userUid ? (
			<ConsoleLayout {...layoutProps}>
				<AddUsers />
				<StatusUsers />
				<InvitedUsers />
				<UploadUsers />
				<UploadResourcesContainer />
				<ReadResources />
			</ConsoleLayout>
		) : (
			<PilalaLoader />
		) : (
			<PilalaLoader />
		);
	}
}

const mapStateToProps = (state) => {
	return {
		userUid: state.userData.uid,
		loginStatus: state.userData.loginStatus
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		setUserData: (value) => dispatch(setUserData(value)),
		setLoginStatus: (value) => dispatch(setLoginStatus(value)),
		fetchDataAction: (actionType, field, value) => dispatch(fetchDataAction(actionType, field, value)),
		resourcesAction: (actionType, value) => dispatch(resourcesAction(actionType, value))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(ConsoleContainer);
