import React, { PureComponent } from 'react';

import { writeResourcesData } from '../uploadResources/API/WriteResources';
import { ENDPONINTS } from '../../../api/Endpoints';

class ReadResources extends PureComponent {
	writeSubsistemas = () => {
		let data = {
			ccf: {
				tarifaPlena: 0.04,
				exonerado1607: 0.04,
				beneficio590: {
					año1: 0.01,
					año3: 0.03,
					año2: 0.02
				}
			},
			pension: {
				tarifaPlena: 0.16,
				licenciaNoRemunerada: 0.16,
				exonerado1607: 0.16
			},
			salud: {
				licenciaNoRemunerada: 0.085,
				tarifaPlena: 0.125,
				exonerado1607: 0.04
			},
			sena: {
				licenciaNoRemunerada: 0,
				beneficio590: {
					año2: 0.01,
					año3: 0.015,
					año1: 0.005
				},
				tarifaPlena: 0.02,
				exonerado1607: 0
			},
			icbf: {
				beneficio590: {
					año2: 0.015,
					año3: 0.0225,
					año1: 0.0075
				},
				tarifaPlena: 0.03,
				licenciaNoRemunerada: 0,
				exonerado1607: 0
			},
			arl: {
				clase3: {
					exonerado1067: 0.02436,
					tarifaPlena: 0.02436,
					tipoRiesgo: 'riesgo medio'
				},
				clase2: {
					exonerado1067: 0.01044,
					tipoRiesgo: 'riesgo bajo',
					tarifaPlena: 0.01044
				},
				clase4: {
					tipoRiesgo: 'riesgo alto',
					exonerado1607: 0.0435,
					tarifaPlena: 0.0435
				},
				clase5: {
					tarifaPlena: 0.0696,
					exonerado1607: 0.0696,
					tipoRiesgo: 'riesgo maximo'
				},
				clase1: {
					exonerado1607: 0.00522,
					tipoRiesgo: 'riesgo minimo',
					tarifaPlena: 0.00522
				}
			}
		};

		let requestData = {
			resourceType: 'subsistemas',
			data: data,
			options: {}
		};

		writeResourcesData(requestData)
			.then((result) => {
				console.log('result', result);
			})
			.catch((error) => {
				console.log('result', error);
			});
	};

	readResources = () => {
		fetch(ENDPONINTS.readResources, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				document: 'subsistemas',
				value: 'empleado'
			})
		})
			.then((response) =>
				response.json().then((data) => ({
					data: data,
					status: response.status
				}))
			)
			.then((result) => {
				console.log('res', result);

				// let items = result.data.data;
				// let csv = '';

				// items.forEach((item, index) => {
				// 	let keysAmount = Object.keys(item).length;

				// 	// If this is the first row, generate the headings
				// 	if (index === 0) {
				// 		// Loop each property of the object

				// 		Object.keys(item).sort().forEach((key, index) => {
				// 			csv += key + (keysAmount !== index + 1 ? ',' : '\r\n');
				// 		});

				// 		Object.keys(item).sort().forEach((key, index) => {
				// 			let value = '';

				// 			if (typeof item[key] === 'string' && item[key].indexOf(',') >= 0) {
				// 				value = `"${item[key]}"`;
				// 			} else {
				// 				value = item[key];
				// 			}

				// 			csv += value + (keysAmount !== index + 1 ? ',' : '\r\n');
				// 		});
				// 	} else {
				// 		Object.keys(item).sort().forEach((key, index) => {
				// 			let value = '';

				// 			if (typeof item[key] === 'string' && item[key].indexOf(',') >= 0) {
				// 				value = `"${item[key]}"`;
				// 			} else {
				// 				value = item[key];
				// 			}

				// 			csv += value + (keysAmount !== index + 1 ? ',' : '\r\n');
				// 		});
				// 	}
				// });

				// // for (let row = 0; row < items.length; row++) {
				// // 	let keysAmount = Object.keys(items[row]).length;

				// // 	// If this is the first row, generate the headings
				// // 	if (row === 0) {
				// // 		// Loop each property of the object

				// // 		Object.keys(items[row]).sort().forEach((key, index) => {
				// // 			csv += key + (keysAmount !== index + 1 ? ',' : '\r\n');
				// // 		});

				// // 		Object.keys(items[row]).sort().forEach((key, index) => {
				// // 			csv += items[row][key] + (keysAmount !== index + 1 ? ',' : '\r\n');
				// // 		});
				// // 	} else {
				// // 		Object.keys(items[row]).sort().forEach((key, index) => {
				// // 			csv += items[row][key] + (keysAmount !== index + 1 ? ',' : '\r\n');
				// // 		});
				// // 	}
				// // }

				// console.log('csv', csv);

				// this.download(csv, 'tiposCotizantes.csv', null);
			});
	};

	download = (content, fileName, mimeType) => {
		var a = document.createElement('a');
		mimeType = mimeType || 'application/octet-stream';

		if (navigator.msSaveBlob) {
			// IE10
			navigator.msSaveBlob(
				new Blob([ content ], {
					type: mimeType
				}),
				fileName
			);
		} else if (URL && 'download' in a) {
			//html5 A[download]
			a.href = URL.createObjectURL(
				new Blob([ content ], {
					type: mimeType
				})
			);
			a.setAttribute('download', fileName);
			document.body.appendChild(a);
			a.click();
			document.body.removeChild(a);
		} else {
			window.location.href = 'data:application/octet-stream,' + encodeURIComponent(content); // only this mime type is supported
		}
	};

	render() {
		return <input type="button" id="launch" value="Read Resources" onClick={this.writeSubsistemas} />;
	}
}

export default ReadResources;
