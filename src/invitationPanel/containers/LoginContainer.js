import React from 'react';
import { connect } from 'react-redux';
import { LOGIN_FORM } from '../../redux/actions/types';
import { setLoginData } from '../../redux/actions/LoginFormActions';
import { AUTH_ERRORS, AUTH_MESSAGES } from '../../constants/constants';
import { FIREBASE_AUTH } from '../../api/Firebase';
import LoginLayout from './LoginLayout';
import UserLoggedLayout from './UserLoggedLayout';
import { signInServerSide } from './../../api/SignInServerSide';
import { setUserData, setLoginStatus } from '../../redux/actions/UserDataActions';
import { fetchDataAction, resourcesAction } from '../../redux/actions/Actions';
import { FETCH_DATA, RESOURCES } from '../../redux/actions/types';

export class LoginContainer extends React.Component {
	setLoginFormData = (event) => {
		switch (event.target.name) {
			case 'email':
				this.props.setLoginData(LOGIN_FORM.SET_EMAIL, event.target.value);
				break;
			case 'password':
				this.props.setLoginData(LOGIN_FORM.SET_PASSWORD, event.target.value);
				break;
			default:
				break;
		}
	};

	handlesubmitLoginForm = async (e) => {
		e.preventDefault();

		this.props.setLoginData(LOGIN_FORM.SET_EMAIL_ERROR, false);
		this.props.setLoginData(LOGIN_FORM.SET_PASSWORD_ERROR, false);

		if (this.props.loginFormEmail && this.props.loginFormPassword) {
			FIREBASE_AUTH.signInWithEmailAndPassword(this.props.loginFormEmail, this.props.loginFormPassword)
				.then(async (user) => {
					const tokenRequest = await FIREBASE_AUTH.currentUser.getIdTokenResult();
					const userEmail = user.user.email;
					const userUid = user.user.uid;

					if (tokenRequest.token && userEmail) {
						signInServerSide(
							{
								email: userEmail
							},
							tokenRequest.token
						)
							.then((result) => {
								console.log('RESULT', result);
								if (result.status === 'SUCCESS' && result.userData && result.userData.uid) {
									if (result.userData.uid === userUid) {
										this.props.setUserData(result.userData);
										this.props.setLoginStatus(true);
										this.props.setLoginData(LOGIN_FORM.RESET_DATA, {});
										this.props.history.push(`${user.user.uid}/fetchData`);
									} else {
										throw new Error('Error en verificación de autenticación en el servicor');
									}
								} else {
									throw new Error('Error en verificación de autenticación en el servicor');
								}
							})
							.catch((error) => {
								console.log('error', error);
								FIREBASE_AUTH.signOut();
							});
					} else {
						FIREBASE_AUTH.signOut();
					}
				})
				.catch((error) => {
					console.log('ERROR', error);
					// Handle Errors here.
					let errorCode = error.code;

					switch (errorCode) {
						case AUTH_ERRORS.USER_NOT_FOUND:
							this.props.setLoginData(LOGIN_FORM.SET_EMAIL_ERROR, AUTH_MESSAGES.USER_NOT_FOUND);

							break;

						case AUTH_ERRORS.WRONG_EMAIL:
							this.props.setLoginData(LOGIN_FORM.SET_EMAIL_ERROR, AUTH_MESSAGES.WRONG_EMAIL);
							break;

						case AUTH_ERRORS.WRONG_PASSWORD:
							this.props.setLoginData(LOGIN_FORM.SET_PASSWORD_ERROR, AUTH_MESSAGES.WRONG_PASSWORD);
							break;

						default:
							break;
					}
				});
		} else {
			if (!this.props.loginFormEmail) {
				this.props.setLoginData(LOGIN_FORM.SET_EMAIL_ERROR, AUTH_MESSAGES.WRONG_EMAIL);
			}
			if (!this.props.loginFormPassword) {
				this.props.setLoginData(LOGIN_FORM.SET_PASSWORD_ERROR, AUTH_MESSAGES.NO_DATA_PASSWORD);
			}
		}
	};

	navigateToConsole = () => {
		this.props.history.push(`${this.props.userUid}/Console`);
	};

	signOut = () => {
		FIREBASE_AUTH.signOut();
		this.props.setLoginStatus(false);
		this.props.setUserData({});
		this.clearReduxOnSignOut();
		// this.props.setSignOutStatus(true)
	};

	clearReduxOnSignOut = () => {
		this.props.fetchDataAction(FETCH_DATA.RESET_REQUEST_STATUS, '', {});
		this.props.resourcesAction(RESOURCES.RESOURCES_RESET_DATA, {});
	};

	render() {
		const layoutProps = {
			loginFormEmailValue: this.props.loginFormEmail,
			loginFormPasswordValue: this.props.loginFormPassword,
			loginFormEmailError: this.props.loginFormEmailError,
			loginFormPasswordError: this.props.loginFormPasswordError,
			setLoginFormData: this.setLoginFormData,
			submitLoginForm: this.handlesubmitLoginForm
		};

		const userLoggedProps = {
			userName: this.props.userName,
			navigateToConsole: this.navigateToConsole,
			signOut: this.signOut
		};
		return this.props.loginStatus ? <UserLoggedLayout {...userLoggedProps} /> : <LoginLayout {...layoutProps} />;
	}
}

const mapStateToProps = (state) => {
	return {
		loginStatus: state.userData.loginStatus,
		loginFormEmail: state.loginForm.email,
		loginFormPassword: state.loginForm.password,
		loginFormEmailError: state.loginForm.emailError,
		loginFormPasswordError: state.loginForm.passwordError,
		userName: state.userData.name,
		userUid: state.userData.uid
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		setLoginData: (actionType, value) => dispatch(setLoginData(actionType, value)),
		setUserData: (value) => dispatch(setUserData(value)),
		setLoginStatus: (value) => dispatch(setLoginStatus(value)),
		fetchDataAction: (actionType, field, value) => dispatch(fetchDataAction(actionType, field, value)),
		resourcesAction: (actionType, value) => dispatch(resourcesAction(actionType, value))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
