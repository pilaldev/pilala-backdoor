import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import { makeStyles, createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

const theme = createMuiTheme({
	palette: {
		primary: {
			main: '#6D29A9'
		},
		secondary: {
			main: '#FF9E00'
		}
	}
});

const useStyles = makeStyles((theme) => ({
	paper: {
		marginTop: theme.spacing(8),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center'
	},
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: '#FF9E00'
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
		color: 'white',
		backgroundColor: '#6D29A9',
		'&:hover': {
			backgroundColor: '#9429A9'
		}
	},
	logOut: {
		color: 'white'
	}
}));

const UserLoggedLayout = (props) => {
	const classes = useStyles();

	return (
		<ThemeProvider theme={theme}>
			<Container component="main" maxWidth="xs">
				<CssBaseline />
				<div className={classes.paper}>
					<Avatar className={classes.avatar} />
					<Typography component="h1" variant="h5">
						Hola {props.userName}
					</Typography>

					<Button
						type="submit"
						fullWidth
						variant="contained"
						className={classes.submit}
						onClick={props.navigateToConsole}
					>
						Go to Console
					</Button>

					<Button
						color="secondary"
						type="submit"
						fullWidth
						variant="contained"
						className={classes.logOut}
						onClick={props.signOut}
					>
						Log Out
					</Button>
				</div>
			</Container>
		</ThemeProvider>
	);
};

export default UserLoggedLayout;
