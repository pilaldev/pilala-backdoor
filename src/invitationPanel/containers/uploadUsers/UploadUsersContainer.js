import React, { Component } from 'react';

import FlatfileImporter from 'flatfile-csv-importer';
import { connect } from 'react-redux';
import { writeUsersData } from '../../../api/WriteUsersData';
import './styles.css';
import { FIRESTORE } from '../../../api/Firebase';
const LICENSE_KEY = 'd55b2a8a-8e65-458d-b6f7-2f9e44be1397';

FlatfileImporter.setVersion(2);

class UploadUsers extends Component {
	constructor(props) {
		super(props);
		this.launch = this.launch.bind(this);
		this.importer = new FlatfileImporter(LICENSE_KEY, {
			title: 'Add Users',
			fields: [
				{
					label: 'codigo',
					key: 'codigo',
					description: 'The full name of the user',
				},
				{
					label: 'logo',
					key: 'logo',
					description: 'The full name of the user',
				},
				{
					label: 'nit',
					key: 'nit',
					description: 'The full name of the user',
				},
				{
					label: 'razonSocial',
					key: 'razonSocial',
					description: 'The full name of the user',
				},
				{
					label: 'subsistema',
					key: 'subsistema',
					description: 'The full name of the user',
				}
				
			],
			type: 'User',
			allowInvalidSubmit: true,
			managed: true,
			allowCustom: true,
			disableManualInput: false
		});
		this.state = {
			results: 'Feedback.',
			failed: []
		};

		// this.importer.registerRecordHook((record, index) => {
		// 	let out = {};
		// 	if (record.companyNit.includes('.')) {
		// 		out.companyNit = {
		// 			value: record.companyNit.replace(/\./g, ''),
		// 			info: [
		// 				{
		// 					message: 'No dots allowed. All dots deleted',
		// 					level: 'warning'
		// 				}
		// 			]
		// 		};
		// 	}
		// 	return out;
		// });

		this.importer.setCustomer({
			userId: this.props.userUid,
			name: this.props.userName + ' ' + this.props.lastName
		});
	}

	launch() {
		this.importer
			.requestDataFromUser()
			.then((results) => {
				this.importer.displayLoader();

				this.writeTemporary(results.validData)
					.then((result) => {
						console.log('al then de afuera', result);
						if (result.existingUsers.length) {
							this.setState({
								results: 'Los siguientes usuarios ya existen: ',
								failed: result.existingUsers
							});

							this.importer.displaySuccess('Algunos usuarios ya existen en tu base de datos');
						} else {
							this.importer.displaySuccess('Success!');
							this.setState({
								results: 'Carga Exitosa'
							});
						}
					})
					.catch((error) => {
						// console.log('Entró por el catch externo');

						this.setState({
							results: error ? JSON.stringify(error) : 'Error de ejecución'
						});
						this.importer.displaySuccess(error.message ? error.message.toString() : 'Error de ejeución');
					});
			})
			.catch((error) => {
				this.importer.displayError('Error en la subida de los datos');
			});
	}

	writeTemporary = (results) => {
		console.log('RESULTS', results, results.length);

		// results = results.filter((user, index, self) => index === self.findIndex((t) => t.code === user.code));
		// let parsedResults = [];

		// results.map((result) => {
		// 	parsedResults.push(result.arl);
		// });

		// console.log('PARSED RESULTS', parsedResults);

		// console.log("PROVINCES", provinces)

		FIRESTORE.collection('recursos')
			.doc('administradoras')
			.set(
				{
					ccf: results
				},
				{ merge: true }
			)
			.then((ref) => {
				console.log('ref', ref);
			});
	};

	UploadUsers = async (results) => {
		return writeUsersData({ userDataArray: results })
			.then((result) => {
				// console.log('resultado', result);
				if (result.status === 'SUCCESS') {
					return result;
				} else {
					// console.log('vino acá');
					throw result;
				}
			})
			.catch((error) => {
				// console.log('ERRORES', error);
				throw error ? error : 'Error de ejecución';
			});
	};

	render() {
		return (
			<div className="upluoadUsersContainer">
				{LICENSE_KEY ? null : (
					<div className="licenseAsk">
						Obtain your license key from the
						<a href="https://flatfile.io/app" /*target="_blank"*/>
							Flatfile Dashboard &rarr;
						</a>
						<p>
							Once you've found it, set the <code>LICENSE_KEY</code> variable on Line 8 before continuing.
						</p>
					</div>
				)}
				<input
					type="button"
					id="launch"
					className={LICENSE_KEY ? null : 'disabled'}
					value="Launch Importer"
					onClick={this.launch}
				/>
				{/* <div className="download">
					<a href="robots.csv" target="_blank" rel="noopener noreferrer">
						Download a sample csv file here
					</a>
				</div> */}
				<textarea id="raw_output" readOnly value={this.state.results + '          ' + this.state.failed} />
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		userUid: state.userData.uid,
		userName: state.userData.name,
		lastName: state.userData.lastName
	};
};

const mapDispatchToProps = (dispatch) => {
	return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(UploadUsers);
