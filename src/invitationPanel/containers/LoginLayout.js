import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles, createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

const Copyright = () => {
	return (
		<Typography variant="body2" color="textSecondary" align="center">
			{'Copyright © '}
			<Link color="inherit" href="https://pilala.co/">
				Pilalá
			</Link>{' '}
			{new Date().getFullYear()}
			{'.'}
		</Typography>
	);
};

const theme = createMuiTheme({
	palette: {
		primary: {
			main: '#6D29A9'
		},
		secondary: {
			main: '#d81b60'
		}
	}
});

const useStyles = makeStyles((theme) => ({
	paper: {
		marginTop: theme.spacing(8),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center'
	},
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: '#FF9E00'
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(1)
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
		color: 'white',
		backgroundColor: '#6D29A9',
		'&:hover': {
			backgroundColor: '#9429A9'
		}
	}
}));

const LoginLayout = (props) => {
	const classes = useStyles();

	return (
		<ThemeProvider theme={theme}>
			<Container component="main" maxWidth="xs">
				<CssBaseline />
				<div className={classes.paper}>
					<Avatar className={classes.avatar} />
					<Typography component="h1" variant="h5">
						Sign in
					</Typography>
					<form
						onSubmit={(e) => {
							props.submitLoginForm(e);
						}}
					>
						<TextField
							error={props.loginFormEmailError ? true : false}
							helperText={props.loginFormEmailError ? props.loginFormEmailError : ''}
							// className={classes.textField}
							variant="outlined"
							margin="normal"
							required
							fullWidth
							id="email"
							label="Email Address"
							name="email"
							autoComplete="email"
							autoFocus
							value={props.loginFormEmailValue}
							onChange={(e) => props.setLoginFormData(e)}
						/>
						<TextField
							error={props.loginFormPasswordError ? true : false}
							helperText={props.loginFormPasswordError ? props.loginFormPasswordError : ''}
							variant="outlined"
							margin="normal"
							required
							fullWidth
							name="password"
							label="Password"
							type="password"
							id="password"
							autoComplete="current-password"
							value={props.loginFormPasswordValue}
							onChange={(e) => props.setLoginFormData(e)}
						/>

						<Button
							type="submit"
							fullWidth
							variant="contained"
							className={classes.submit}
							onClick={(e) => {
								props.submitLoginForm(e);
							}}
						>
							Sign In
						</Button>
					</form>
				</div>
				<Box mt={8}>
					<Copyright />
				</Box>
			</Container>
		</ThemeProvider>
	);
};

export default LoginLayout;
