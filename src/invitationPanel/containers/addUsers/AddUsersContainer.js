import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setAddUserData, setErrorMessage, resetAddUserData } from '../../../redux/actions/AddUserFormActions';
import { ADD_USER_FORM_ERROR_MESSAGES, REG_EXPS } from '../../../constants/constants';

import { writeUsersData } from '../../../api/WriteUsersData';

import AddUsersLayout from './AddUsersLayout';

export class AddUsers extends Component {
	setFormData = (event) => {
		let validation = true;

		console.log('FORM', event);

		if (event.target.value) {
			switch (event.target.name) {
				case 'fullName':
					validation = REG_EXPS.name.test(event.target.value);
					break;

				case 'documentNumber':
					validation = REG_EXPS.numbers.test(event.target.value);
					break;

				case 'phoneNumber':
					validation = REG_EXPS.numbers.test(event.target.value);
					break;

				case 'companyNit':
					validation = REG_EXPS.numbers.test(event.target.value);
					break;

				default:
					break;
			}
		}

		if (validation) {
			this.props.setAddUserData(event.target.name, event.target.value);
		}
	};

	validateFields = () => {
		let nameField = true;
		let emailField = true;
		let documentNumberField = true;
		let phoneNumberField = true;
		let companyNitField = true;

		if (!this.props.addUserFormNameValue) {
			this.props.setErrorMessage('fullName', ADD_USER_FORM_ERROR_MESSAGES.FIELD_REQUIRED);
			nameField = false;
		}

		if (!this.props.addUserFormEmailValue) {
			this.props.setErrorMessage('email', ADD_USER_FORM_ERROR_MESSAGES.FIELD_REQUIRED);
			emailField = false;
		} else {
			if (!REG_EXPS.email.test(this.props.addUserFormEmailValue)) {
				this.props.setErrorMessage('email', 'Por favor verifique su email');
				emailField = false;
			}
		}

		if (!this.props.addUserFormDocumentNumberValue) {
			this.props.setErrorMessage('documentNumber', ADD_USER_FORM_ERROR_MESSAGES.FIELD_REQUIRED);
			documentNumberField = false;
		}

		if (!this.props.addUserFormPhoneNumberValue) {
			this.props.setErrorMessage('phoneNumber', ADD_USER_FORM_ERROR_MESSAGES.FIELD_REQUIRED);
			phoneNumberField = false;
		}

		if (!this.props.addUserFormCompanyNitValue) {
			this.props.setErrorMessage('companyNit', ADD_USER_FORM_ERROR_MESSAGES.FIELD_REQUIRED);
			companyNitField = false;
		}

		return nameField & emailField & documentNumberField & phoneNumberField & companyNitField;
	};

	submitAddUserForm = async (e) => {
		e.preventDefault();

		const checkFields = this.validateFields();

		if (checkFields) {
			let userDataArray = [
				{
					email: this.props.addUserFormEmailValue,
					name: this.props.addUserFormNameValue,
					documentType: this.props.addUserFormDocumentTypeValue,
					documentNumber: this.props.addUserFormDocumentNumberValue,
					phoneNumber: this.props.addUserFormPhoneNumberValue,
					companyNit: this.props.addUserFormCompanyNitValue,
					location: {
						city: this.props.addUserFormCityValue,
						province: this.props.addUserFormProvinceValue
					},
					featureFlags: this.props.addUserFormFeatureFlagsValue,
					genre: this.props.addUserFormGenreValue
				}
			];
			writeUsersData({ userDataArray })
				.then((result) => {
					if (result.existingUsers.length) {
						this.props.setErrorMessage('email', ADD_USER_FORM_ERROR_MESSAGES.USER_EXISTS);
					} else if (result.status === 'SUCCESS') {
						this.props.resetAddUserData();
					}
				})
				.catch((error) => {
					this.props.setErrorMessage('email', ADD_USER_FORM_ERROR_MESSAGES.REQUEST_FAILED);
				});
		}
	};

	render() {
		const layoutProps = {
			setFormData: this.setFormData,
			addUserFormNameValue: this.props.addUserFormNameValue,
			addUserFormEmailValue: this.props.addUserFormEmailValue,
			addUserFormDocumentNumberValue: this.props.addUserFormDocumentNumberValue,
			addUserFormDocumentTypeValue: this.props.addUserFormDocumentTypeValue,
			addUserFormPhoneNumberValue: this.props.addUserFormPhoneNumberValue,
			addUserFormCompanyNitValue: this.props.addUserFormCompanyNitValue,
			addUserFormErrorMessages: this.props.addUserFormErrorMessages,
			submitAddUserForm: this.submitAddUserForm
		};
		return <AddUsersLayout {...layoutProps} />;
	}
}

const mapStateToProps = (state) => {
	return {
		addUserFormNameValue: state.addUserForm.fullName,
		addUserFormEmailValue: state.addUserForm.email,
		addUserFormDocumentNumberValue: state.addUserForm.documentNumber,
		addUserFormDocumentTypeValue: state.addUserForm.documentType,
		addUserFormPhoneNumberValue: state.addUserForm.phoneNumber,
		addUserFormCompanyNitValue: state.addUserForm.companyNit,
		addUserFormErrorMessages: state.addUserForm.errorMessages,
		addUserFormCityValue: state.addUserForm.city,
		addUserFormProvinceValue: state.addUserForm.province,
		addUserFormFeatureFlagsValue: state.addUserForm.featureFlags,
		addUserFormGenreValue: state.addUserForm.genre
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		setAddUserData: (field, value) => dispatch(setAddUserData(field, value)),
		setErrorMessage: (field, value) => dispatch(setErrorMessage(field, value)),
		resetAddUserData: () => dispatch(resetAddUserData())
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(AddUsers);
