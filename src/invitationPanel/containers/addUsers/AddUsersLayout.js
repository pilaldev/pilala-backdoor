import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { InputAdornment, MenuItem } from '@material-ui/core';
import { makeStyles, createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { ID_VALUES } from '../../../constants/constants';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { useSelector, useDispatch } from 'react-redux';
import { setAddUserData } from '../../../redux/actions/AddUserFormActions';
// import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Checkbox from '@material-ui/core/Checkbox';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import GridList from '@material-ui/core/GridList';
import { addUserFormAction } from '../../../redux/actions/Actions';
import { ADD_USER_DATA_FORM } from '../../../redux/actions/types';

const theme = createMuiTheme({
	palette: {
		primary: {
			main: '#6D29A9'
		},
		secondary: {
			main: '#d81b60'
		}
	}
});

const useStyles = makeStyles((theme) => ({
	paper: {
		marginTop: theme.spacing(1),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center'
	},
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: '#FF9E00'
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(1)
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
		color: 'white',
		backgroundColor: '#6D29A9',
		'&:hover': {
			backgroundColor: '#9429A9'
		}
	},
	itemList: {
		cursor: 'pointer'
	}
}));

const CheckBoxList = (props) => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const featureFlags = useSelector((state) => state.addUserForm.featureFlags);

	const setFeatureFlag = (field) => {
		dispatch(addUserFormAction(ADD_USER_DATA_FORM.SET_FEATURE_FLAG_VALUE, { field }));
	};

	return (
		<GridList cellHeight={20} cols={2}>
			{Object.keys(featureFlags).map((featureFlag) => {
				return (
					<ListItem
						id={featureFlag}
						className={classes.itemList}
						key={featureFlag}
						onClick={() => setFeatureFlag(featureFlag)}
					>
						<ListItemIcon>
							<Checkbox edge="start" checked={featureFlags[featureFlag]} disableRipple />
						</ListItemIcon>
						<ListItemText primary={`${featureFlag}`} />
					</ListItem>
				);
			})}
		</GridList>
	);
};

const AddUsersLayout = (props) => {
	const classes = useStyles();
	const dispatch = useDispatch();

	const CITIES = useSelector((state) => state.resources.cities);
	const PROVINCES = useSelector((state) => state.resources.provinces);
	const GENRES = [ { display: 'Masculino', value: 1 }, { display: 'Femenino', value: 1 } ];

	const getListValue = (event, value, reason) => {
		const fieldId = event.target.id.split('-')[0];

		switch (fieldId) {
			case 'province':
				dispatch(setAddUserData(fieldId, value));

				break;

			case 'city':
				dispatch(setAddUserData(fieldId, value));
				break;
				
			case 'genre':
				dispatch(setAddUserData(fieldId, value));
				break;

			default:
				return;
		}
	};

	return (
		<ThemeProvider theme={theme}>
			<Container component="main" maxWidth="xs">
				<CssBaseline />
				<div className={classes.paper}>
					<Avatar className={classes.avatar} />
					<Typography component="h1" variant="h5">
						Agregar Usuario
					</Typography>
					<form
						onSubmit={(e) => {
							e.preventDefault();
							props.submitAddUserForm(e);
						}}
					>
						<TextField
							error={props.addUserFormErrorMessages.fullName ? true : false}
							helperText={
								props.addUserFormErrorMessages.fullName ? props.addUserFormErrorMessages.fullName : ''
							}
							variant="outlined"
							margin="normal"
							required
							fullWidth
							id="name"
							label="Full Name"
							name="fullName"
							autoComplete="name"
							autoFocus
							value={props.addUserFormNameValue}
							onChange={props.setFormData}
						/>

						<Autocomplete
							id="province"
							options={PROVINCES}
							getOptionLabel={(option) => option.display}
							// value={userProvince.value}
							renderInput={(params) => (
								<TextField
									{...params}
									variant="outlined"
									margin="normal"
									required
									fullWidth
									label="province"
									name="province"
								/>
							)}
							name="province"
							onChange={getListValue}
						/>

						<Autocomplete
							id="city"
							options={CITIES}
							getOptionLabel={(option) => option.display}
							// value={userCity.display}
							renderInput={(params) => (
								<TextField
									{...params}
									variant="outlined"
									margin="normal"
									required
									fullWidth
									label="city"
									name="city"
								/>
							)}
							name="city"
							onChange={getListValue}
						/>

						<Autocomplete
							id="genre"
							options={GENRES}
							getOptionLabel={(option) => option.display}
							// value={userCity.display}
							renderInput={(params) => (
								<TextField
									{...params}
									variant="outlined"
									margin="normal"
									required
									fullWidth
									label="genre"
									name="genre"
								/>
							)}
							name="city"
							onChange={getListValue}
						/>

						<TextField
							error={props.addUserFormErrorMessages.email ? true : false}
							helperText={
								props.addUserFormErrorMessages.email ? props.addUserFormErrorMessages.email : ''
							}
							variant="outlined"
							margin="normal"
							required
							fullWidth
							id="email"
							label="Email Address"
							name="email"
							autoComplete="email"
							value={props.addUserFormEmailValue}
							onChange={props.setFormData}
						/>

						<TextField
							error={props.addUserFormErrorMessages.documentNumber ? true : false}
							helperText={
								props.addUserFormErrorMessages.documentNumber ? (
									props.addUserFormErrorMessages.documentNumber
								) : (
									''
								)
							}
							variant="outlined"
							margin="normal"
							required
							fullWidth
							id="documentNumber"
							label="Document Number"
							name="documentNumber"
							// autoComplete="documentNumber"
							value={props.addUserFormDocumentNumberValue}
							onChange={props.setFormData}
							InputProps={{
								startAdornment: (
									<InputAdornment position="start">
										<TextField
											select={true}
											InputProps={{
												disableUnderline: true
											}}
											value={props.addUserFormDocumentTypeValue}
											onChange={props.setFormData}
											name="documentType"
										>
											{ID_VALUES.map((item) => (
												<MenuItem key={item} value={item}>
													{item}
												</MenuItem>
											))}
										</TextField>
									</InputAdornment>
								)
							}}
						/>

						<TextField
							error={props.addUserFormErrorMessages.phoneNumber ? true : false}
							helperText={
								props.addUserFormErrorMessages.phoneNumber ? (
									props.addUserFormErrorMessages.phoneNumber
								) : (
									''
								)
							}
							variant="outlined"
							margin="normal"
							required
							fullWidth
							type="tel"
							id="phoneNumber"
							label="Phone Number"
							name="phoneNumber"
							autoComplete="tel"
							value={props.addUserFormPhoneNumberValue}
							onChange={props.setFormData}
						/>

						<TextField
							error={props.addUserFormErrorMessages.companyNit ? true : false}
							helperText={
								props.addUserFormErrorMessages.companyNit ? (
									props.addUserFormErrorMessages.companyNit
								) : (
									''
								)
							}
							variant="outlined"
							margin="normal"
							required
							fullWidth
							type="text"
							id="companyNit"
							label="Company Nit"
							name="companyNit"
							value={props.addUserFormCompanyNitValue}
							onChange={props.setFormData}
						/>

						<CheckBoxList />

						<Button
							type="submit"
							fullWidth
							variant="contained"
							className={classes.submit}
							onClick={(e) => {
								props.submitAddUserForm(e);
							}}
						>
							Add User
						</Button>
					</form>
				</div>
				{/* <Box mt={8}>
					<Copyright />
				</Box> */}
			</Container>
		</ThemeProvider>
	);
};

export default AddUsersLayout;
